import { NextFunction } from "express";
import { User } from "../entity/User";
import { statisticsService } from "../services/statisticsService";

export class StatisticsController {
    async getAllViews(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            statisticsService.getAllViews(user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async addView(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            statisticsService.addView(user, request.body).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getViewsDate(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            statisticsService.getViewsDate(user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getReg(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            statisticsService.getViewsDate(user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getRegsDate(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            statisticsService.getRegsDate(user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }


}