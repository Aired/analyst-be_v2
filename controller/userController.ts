import { NextFunction } from "express";
import { User } from "../entity/User";
import { userService } from "../services/userService";


export class UserController {

    constructor() {
    }

    async drago(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            console.log('User authorized: ', user.auth);
            resolve({ response: 'drago' });
        });
    }

    async test(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.test(request, response, next, user, options).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }


    /* async createUser(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
         return new Promise<any>((resolve, reject) => {
             userService.createUser(user, request.body).then(
                 resp => {
                     console.log(resp);
                     resolve(resp);
                 }
             ).catch(
                 err => reject(err)
             );
         });
     }*/

    async deleteUser(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.deleteUser(user, Number(request.headers.id)).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getUser(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getUser(user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }


    async lolloman2(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getAllUser(user, request.params.codml).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getUserByUsername(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getUserByUsername(user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async loginUser(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            console.log('User: ', user);
            console.log('body: ', request.body);
            userService.loginUser(user, request.body).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async updateUser(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        console.log(request.body);
        return new Promise<any>((resolve, reject) => {
            userService.updateUser(user, request.body).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async updatePassword(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.updatePassword(user, request.body).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async checkMail(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.checkMail(user, request.headers.email).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async requestResetPassword(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.requestResetPassword(user, request.params.email).then( // to change in request.params.value -> CHECK
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async resetPassword(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.resetPassword(user, request.params.otp).then( // to change in request.params.value -> CHECK
                resp => {
                    console.log(resp);
                    // SEND HTML PAGE
                    response.sendFile('/home/antonello4/exor-mail-server/analyst-be/service/reset/reset.html');
                }
            ).catch(
                err => response.sendFile('/home/antonello4/exor-mail-server/analyst-be/service/reset/error.html')
            );
        });
    }

    async resetPasswordDef(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.resetPasswordDef(user, request.params.otp, request.params.password).then( // to change in request.params.value -> CHECK
                resp => {
                    console.log(resp);
                    // SEND HTML PAGE
                    response.send({ 'ok': 'ok' });
                }
            ).catch(
                err => response.send({ 'lol': response })
            );
        });
    }

    async viewResetPassword(request: any, response: any, next: NextFunction, user: User, options: any) {
        response.sendFile('/home/antonello4/exor-mail-server/analyst-be/service/reset/requestReset.html');
    }

    /*------------ NEW FEATURE ------------------ TO ADD ROUTES */
    async getUsersbyWorkProfile(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getUsersbyWorkProfile(user).then( // to change in request.params.value -> CHECK
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async userActivation(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.userActivation(user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async sendVerificationMail(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.sendVerificationMail(user).then( //  -- ADD MAIL TO SEND THE EMAIL
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async regUser(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.regUser(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async activationUser(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.createUser(String(request.params.token)).then(
                resp => {
                    console.log(resp);
                    //resolve(resp);
                    response.send(`
                    <html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>
Unsubscribe
</title>
<style type="text/css">
.input{
    box-shadow: 0 0 0 0 #37bd83, 0 0 0 0 #40dc98;
    text-shadow: none;
    border: none;
    line-height: calc((1rem * 1.25) + 4px);
    border-radius: 5px;
    width: 600px;
    height: 50px;
    color: black;
    border: 2px solid grey;
    padding: 10px;
}
.save{
        background-image: linear-gradient(to right, #40dcb2, #40dc7e);
    box-shadow: 0 0 0 0 #37bd83, 0 0 0 0 #40dc98;
    text-shadow: none;
    border: none;
    line-height: calc((1rem * 1.25) + 4px);
    border-radius: 5px;
    width: 600px;
    height: 50px;
    color: white;
}
</style>
</head>
<body>
<br>
<br>
<div id="div1" class="container">
<div>
<p style="color: black;font-size: 18px;font-weight: bold">Exor Newsletter</p><br>
</div>
<span>
<p style="color: #a4abb3;font-size: 18px;font-weight: bold;font-style: italic">
“Thank you for your interest in EXOR.
We confirm you have been added to EXOR's Newsletter and will now be able to receive all
relevant news and press releases.
Please note that you can unsubscribe anytime through the following link: 
<a class="save" href="https://wwwtools.exor.com/mail/serveunsubscribe/${resp.mailing_list}">‘Unsubscribe’</a> ”<br>
<br>
</p>
</span>
<br>

</div>
</body>

</html>
`);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async addJobProfile(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.addJobProfile(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async deleteJobProfile(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        var url = require('url');
        var url_parts = url.parse(request.url, true);
        var query = url_parts.query;
        return new Promise<any>((resolve, reject) => {
            userService.deleteJobProfile(query.id, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getJobProfile(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getJobProfile(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async testMail(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.testMail().then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getUserProcedure(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getUserProcedure(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }


    async createAdminUser(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.createAdminUser(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async updateAdminUser(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.updateAdminUser(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async deleteAdminUser(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.deleteAdminUser(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getAdminUsers(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            userService.getAdminUsers(request, user).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }


}