import { NextFunction } from "express";
import { User } from "../entity/User";
import { SubscriptionService } from "../services/subscriptionService";
const path = require('path');
export class SubscriptionController {

    constructor() {
    }

    async deleteSubscription(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            console.log(request.params);
            console.log('Access to operation DELETE SUBSCRITPTION');
            SubscriptionService.deleteSubscription(user, request.params.user_id, request.params.mailing_list, request.params.job_profile).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async deleteSubscriptionAdmin(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            var url = require('url');
            var url_parts = url.parse(request.url, true);
            var query = url_parts.query;
            console.log('Access to operation DELETE SUBSCRITPTION ADMIN');
            console.log(request.params);
            SubscriptionService.deleteSubscriptionAdmin(user, query.iduser, query.idmail, query.job_profile).then(
                resp => {
                    console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async getMailinglist(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        console.log(request);
        console.log(user);
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.getMailinglist(user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }
    async lolloman(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.getMailinglist(user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }
    async getSubById(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        console.log(request.params.userId);
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.getSubById(user, request.params.userId).then( // DA VEDERE, DA CAMBIARE IN -> request.params.userId ? - OLD : request.swagger.params['userId'].value
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async addSub(request: any, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.addSub(user, request.body).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async sendMailbySubsId(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.sendMailbySubsId(user, request.body).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async sendMail(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.sendMail(user, request.body).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }
    

    async addMailingList(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.addMailingList(user, request.body).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async deleteMailingList(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        var url = require('url');
        var url_parts = url.parse(request.url, true);
        var query = url_parts.query;
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.deleteMailingList(user, query.id).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

    async unsubscribe(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.unsubscribe(request, user).then(
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }
    async generalUnsubscribe(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.generalUnsubscribe(request, user).then( // TO CHANGE
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }
        async serveUnsubscribe(request: any, response: any, next: NextFunction, user: User, options: any): Promise<any> {
            let mailing_list = request.params.id_mailing_list;
           response.send(`<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>
Unsubscribe
</title>
<style type="text/css">
.input{
    box-shadow: 0 0 0 0 #37bd83, 0 0 0 0 #40dc98;
    text-shadow: none;
    border: none;
    line-height: calc((1rem * 1.25) + 4px);
    border-radius: 5px;
    width: 600px;
    height: 50px;
    color: black;
    border: 2px solid grey;
    padding: 10px;
}
.save{
        background-image: linear-gradient(to right, #40dcb2, #40dc7e);
    box-shadow: 0 0 0 0 #37bd83, 0 0 0 0 #40dc98;
    text-shadow: none;
    border: none;
    line-height: calc((1rem * 1.25) + 4px);
    border-radius: 5px;
    width: 600px;
    height: 50px;
    color: white;
}
</style>
</head>
<body>
<br>
<br>
<div id="div1" class="container">
<div>
<p style="color: black;font-size: 18px;font-weight: bold">Exor Newsletter</p><br>
</div>
<span>
<p style="color: #a4abb3;font-size: 18px;font-weight: bold;font-style: italic">
You are about to remove your subscription to the selected newsletter.</p>
<p style="color: #a4abb3;font-size: 18px;font-weight: bold;font-style: italic">
Enter your e-mail address and an e-mail will be sent to confirm the notification.</p>
</span>
<br>
<span>
<p>Email<br><input type="text" id="email_1" class="input"></p><br>
<!-- <p>Repeat email: <br><input type="text" id="email_2" class="input"></p><br> -->
</span>
<button class="save" onClick="save()">Unsubscribe</button>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
//window.onload = onInit;

//function onInit(){
    //document.getElementById('email_1').innerHTML = window.location.search.substr(1)
//};
function save() {
    var email_1 = document.getElementById('email_1').value;
    //var email_2 = document.getElementById('email_2').value;
   // if (email_1 === email_2) {
        // CALL REQUEST RESET PASSWORD
     /*   $.ajax({
            contentType: 'application/json',
            body: {
                "email": email_1
            },
            dataType: 'json',
            success: function (body) {
                console.log("yo");
            },
            error: function () {
                console.log("lol");
            },
            processData: false,
            type: 'GET',
            url: 'https://dwh.serfit.eu/dragon/user/reset/'+email_1
        });*/
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
           console.log("ok");
           alert('you have properly unsubscribe');
          } else {
               console.log("ko");
          }
        };
        var url = "https://wwwtools.exor.com/mail/generalunsubscribe/${mailing_list}/"+email_1;
        xhttp.open("GET", url, true);
        xhttp.send();
        
  //  } else {
    //    alert('Email do not match');
    //}
}
</script>
</html>`);
            //response.sendFile('C:/Users/Antonello/Documents/2018/Development/analyst-be_v2/services/uns/requestReset.html?mailinglist='+mailing_list);
        /*return new Promise<any>((resolve, reject) => {
            SubscriptionService.unsubscribe(request, user).then( // TO CHANGE
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });*/
    }
    async drago(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        var url = require('url');
        var url_parts = url.parse(request.url, true);
        var query = url_parts.query;
        return new Promise<any>((resolve, reject) => {
            SubscriptionService.getSubById(user, query.userId).then( // DA VEDERE, DA CAMBIARE IN -> request.params.userId ? - OLD : request.swagger.params['userId'].value
                resp => {
                    //console.log(resp);
                    resolve(resp);
                }
            ).catch(
                err => reject(err)
            );
        });
    }

}