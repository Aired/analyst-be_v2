export class ErrorApp extends Error {
    error: string;
    status: number;
}