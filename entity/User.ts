export class User {
    id: number;
    name: string;
    surname: string;
    email: string;
    company: string;
    role: number;
    auth: boolean;

    /*constructor(name: string, surname: string, auth: boolean) {
        this.name = name;
        this.surname = surname;
        this.auth = auth;
    }*/

}