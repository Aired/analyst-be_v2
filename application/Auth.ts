import { Request } from "express";
import { User } from "../entity/User";
let pool = require("../services/database");

var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

export class Auth {

    static currentUser(request: Request): User {

        let user = new User();
        if (request && request.headers && request.headers.token) {
            user = this.splitToken(request.headers.token);
        } else
            if (request.path !== '/user/login') {
                console.log('No token');
                if(request.path == '/mail/list' || request.path == '/user/jobprofiles' || (request.path.search(/unsubscribe/g) !== -1)){
                    user.auth = true;
                } else {
                    user.auth = false;
                }
               
            } else {
                console.log('Login operation');
                user.auth = true;
            }
        return user;
    }

    /*------------- FUNCTIONS ----------------*/
    static diff_minutes(dt2: Date, dt1: Date) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= 60;
        return Math.abs(Math.round(diff));

    }
    static decrypt(text) {
        try {
            var decipher = crypto.createDecipher(algorithm, password)
            var dec = decipher.update(text, 'hex', 'utf8')
            dec += decipher.final('utf8');
        } catch (e) {
            var dec = null;
        }
        return dec;
    }

    static splitToken(token) {
        let user = new User();
        try {
            var data = Auth.decrypt(token);
            var array_data = data.split('|');
            user.id = array_data[0];
            user.name = array_data[1];
            user.surname = array_data[2];
            user.email = array_data[3];
            user.company = array_data[4];
            user.role = array_data[5];
            let temp_date = Date.parse(array_data[6]);
            let exp_date = new Date(temp_date);
            let now = new Date();
            var diff = (exp_date.getTime() - now.getTime()) / 1000;
            diff /= 60;
            diff = Math.abs(Math.round(diff));
            console.log('Exp Token: ', Number(diff));
            if (diff <= 0 || (exp_date < now)) {
                user.auth = false;
            } else {
                user.auth = true;
            }
        } catch (e) {
            console.log('Error split token: ', e)
            user.auth = false;
        }
        return user;
    }

    /* OLD DATA
if(request.headers.token) {
                 user = this.splitToken(request.headers.token);
              } else {
                  console.log('No token');
                  user.auth = true;
              }
              
                          if(request.headers.token && (request.path !== '/user/login')){
              console.log('Primo if');
              
          } else {
              console.log('else 1');
              if(request.path !== '/user/login'){
                  console.log('secondo if');
                  user.auth = false;
              } else user.auth = true;       
          }
    */
}