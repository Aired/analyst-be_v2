import { UserController } from "./controller/userController";
import { SubscriptionController } from "./controller/subscritptionController";
import { StatisticsController } from "./controller/statisticsController";

export const Routes = [
    {
        method: "get",
        route: "/drago",
        controller: UserController,
        action: "drago"
    },
    {
        method: "get",
        route: "/test",
        controller: UserController,
        action: "test"
    },
    /*---------------- USER ROUTES ----------------*/
    {
        method: "post",
        route: "/user",
        controller: UserController,
        action: "regUser"
    },
    {
        method: "get",
        route: "/user/activation/:token",
        controller: UserController,
        action: "activationUser"
    },

    {
        method: "put",
        route: "/user",
        controller: UserController,
        action: "updateUser"
    },
    {
        method: "delete",
        route: "/user",
        controller: UserController,
        action: "deleteUser"
    },
    {
        method: "delete",
        route: "/user/reset/:email",
        controller: UserController,
        action: "requestResetPassword"
    },
    {
        method: "get",
        route: "/user/info",
        controller: UserController,
        action: "getUser"
    },
    {
        method: "get",
        route: "/user/infos/:codml",
        controller: UserController,
        action: "lolloman2"
    },
    {
        method: "get",
        route: "/user/admin",
        controller: UserController,
        action: "getAdminUsers"
    },
    {
        method: "post",
        route: "/user/admin",
        controller: UserController,
        action: "createAdminUser"
    },
    {
        method: "put",
        route: "/user/admin",
        controller: UserController,
        action: "updateAdminUser"
    },
    {
        method: "delete",
        route: "/user/admin",
        controller: UserController,
        action: "deleteAdminUser"
    },
    {
        method: "post",
        route: "/user/login",
        controller: UserController,
        action: "loginUser"
    },
    {
        method: "post",
        route: "/user/update",
        controller: UserController,
        action: "updateUser"
    },
    {
        method: "post",
        route: "/user/password",
        controller: UserController,
        action: "updatePassword"
    },
    {
        method: "get",
        route: "/user/checkmail",
        controller: UserController,
        action: "checkMail"
    },
    {
        method: "get",
        route: "/user/jobprofiles",
        controller: UserController,
        action: "getJobProfile"
    },
    {
        method: "post",
        route: "/user/jobprofiles",
        controller: UserController,
        action: "addJobProfile"
    },
    {
        method: "delete",
        route: "/user/jobprofiles",
        controller: UserController,
        action: "deleteJobProfile"
    },
    /*----------------- SUBSCRIPTION ROUTES ---------------------*/
   {
        method: "delete",
        route: "/mail/subs",
        controller: SubscriptionController,
        action: "deleteSubscriptionAdmin"
    },
    {
        method: "get",
        route: "/mail/unsubscribe/:mailing_list/:job_profile/:user_id",
        controller: SubscriptionController,
        action: "deleteSubscription"
    },
    {
        method: "get",
        route: "/mail/generalunsubscribe/:mailing_list/:mail",
        controller: SubscriptionController,
        action: "generalUnsubscribe"
    },
    {
        method: "get",
        route: "/mail/serveunsubscribe/:id_mailing_list",
        controller: SubscriptionController,
        action: "serveUnsubscribe"
    },
    {
        method: "get",
        route: "/mail/list",
        controller: SubscriptionController,
        action: "lolloman"
    },
    {
        method: "get",
        route: "/mail/subs/:userId",
        controller: SubscriptionController,
        action: "getSubById"
    },
    {
        method: "post",
        route: "/mail/subs",
        controller: SubscriptionController,
        action: "addSub"
    },
    {
        method: "post",
        route: "/mail/send",
        controller: SubscriptionController,
        action: "sendMail" // sendMailbySubsId , in test
    }, 
    {
        method: "post",
        route: "/mail/mailinglist", // CHANGE FROM addmail -> avvisare bramo
        controller: SubscriptionController,
        action: "addMailingList"
    },
    {
        method: "delete",
        route: "/mail/mailinglist", // CHANGE FROM addmail -> avvisare bramo
        controller: SubscriptionController,
        action: "deleteMailingList"
    },
    {
        method: "get",
        route: "/mail/testmail",
        controller: UserController,
        action: "testMail"
    },
    
    /*-------------------- STATISTICS ROUTES --------------------*/
    {
        method: "get",
        route: "/statistics/views",
        controller: StatisticsController,
        action: "getAllViews"
    },
    {
        method: "post",
        route: "/statistics/views",
        controller: StatisticsController,
        action: "addView"
    },
    {
        method: "get",
        route: "/statistics/viewsfordate",
        controller: StatisticsController,
        action: "getViewsDate"
    },
    {
        method: "get",
        route: "/statistics/regs",
        controller: StatisticsController,
        action: "getReg"
    },
    {
        method: "get",
        route: "/statistics/regsfordate",
        controller: StatisticsController,
        action: "getRegsDate"
    },
    /*-------------------------- PASSWORD RESET ---------------------*/
    {
        method: "get",
        route: "/files/requestReset/:otp",
        controller: UserController,
        action: "resetPassword"
    },
    {
        method: "get",
        route: "/files/defreset/:otp/:password",
        controller: UserController,
        action: "resetPasswordDef"
    },
    {
        method: "get",
        route: "/files/reset",
        controller: UserController,
        action: "viewResetPassword"
    },
    {
        method: "get",
        route: "/procedure/users",
        controller: UserController,
        action: "getUserProcedure"
    }

]