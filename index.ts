import * as express from "express";
import { Routes } from "./routes";
import { Request, Response } from "express";
//import { ERRORS_CODE } from "./constants/errorCode";
import { Auth } from "./application/Auth";
import { ErrorApp } from "./entity/Error";

const cluster = require('cluster');
const http = require('https');
const numCPUs = require('os').cpus().length;
let bodyParser = require('body-parser');
var forceSsl = require('express-force-ssl');

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

var cors = require('cors');
app.use(cors());

//app.use(forceSsl);
/*if (cluster.isMaster) {
    console.log(`Master ${process.pid} is running`);
    
    // Fork workers.
    for (let i = 0; i < (numCPUs); i++) {
        cluster.fork();
    }
    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`);
    });
    console.log('Server started on -> http://localhost:3000');
} else {*/
    Routes.forEach(route => {

        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next, Auth.currentUser(req));
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined)
                    .catch((err: ErrorApp) => {
                        console.log('generic error', err);
                        // TODO print generic error
                        //res.sendStatus(err.status);
                        //res.send(err.status)
                        //res.send({ ERROR: err.error });
                        res.status(Number(err.status)).json({ error: String(err.error) });
                    });

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    app.listen(3000);
    console.log('Server started on -> http://localhost:3000');

    
//console.log(`Worker ${process.pid} started`); }
