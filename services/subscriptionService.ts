import { User } from "../entity/User";

let pool = require("../services/database");

export class SubscriptionService {

  static copyTest(user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }
  static deleteSubscription(user: User, iduser, idmail, jobprofile): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        var sql = "DELETE FROM `subscriptions` WHERE iduser = ? AND codml = ? AND job_profile = ?";
        pool.query(sql, [iduser, idmail, jobprofile], function (err, result, fields) {
          if (err) {
            console.log('query mysql error.'); // consol log err for more details
            reject({ error: err, status: 500 });
          } else {
            resolve({ 'Subscription': 'OFF' });
            console.log('subscription deleted');
          }//else query
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static deleteSubscriptionAdmin(user: User, iduser, idmail, jobprofile): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        var sql = "DELETE FROM `subscriptions` WHERE iduser = ? AND codml = ? AND job_profile = ?";
        pool.query(sql, [iduser, idmail, jobprofile], function (err, result, fields) {
          if (err) {
            console.log('query mysql error.'); // consol log err for more details
            reject({ error: err, status: 500 });
          } else {
            resolve({ 'Subscription': 'OFF' });
            console.log('subscription deleted');
          }//else query
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static getMailinglist(user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        pool.query("SELECT * FROM mailing_list", function (err, result, fields) {
          if (err) {
            console.log('query mysql error'); // consol log err for more details
            reject({ error: err, status: 500 });
          } else {
            if (Object.keys(result).length > 0) {
              resolve(result);
            } else {
              resolve({ result: 'no result', status: 404 });
            }
          } //else connection trow error
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static getSubById(user: User, userId): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        pool.query("SELECT codml AS idmailinglist FROM subscriptions WHERE iduser = ?", [userId], function (err, result, fields) {
          if (err) {
            console.log('query mysql error.'); // consol log err for more details
            reject({ error: err, status: 500 });
          } else {
            if (Object.keys(result).length > 0) {
              resolve(result);
            } else {
              resolve({ result: 'no result', status: 404 });
            }
          } //else query mysql
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static addSub(user: User, body): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        let iduser =  body.iduser;
        let idmail = body.idmail;
        let job_profile = body.job_profile;
        let company = body.company;
        //var sql = "INSERT INTO subscriptions (iduser, codml, job_profile) SELECT * FROM (SELECT ? AS iduser1, ? AS idmail, ? AS jobprofile) AS tmp WHERE NOT EXISTS (SELECT iduser FROM subscriptions WHERE iduser = iduser1 AND codml = idmail AND job_profile = jobprofile) LIMIT 1;";
        var sql = `INSERT INTO subscriptions (iduser, codml, job_profile, company) VALUES (?,?,?,?)`;
        console.log('SQL: ', sql);
        console.log('Params: ', body);
        pool.query(sql, [iduser, idmail, job_profile, company], function (err, result, fields) {
          if (err) {
            console.log('query mysql error.');
            let error = JSON.stringify(err);
            if (error.search(/ER_DUP_ENTRY/g) !== -1) {
              reject({ error: 'Subscription already exist', status: 406 })
            }  else {
              reject({ error: err, status: 400 });
            }
          } else {
            resolve({ 'SUBSCRITPION': 'ADDED' });
          }
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  /*
  * TO DO
  */
  static sendMailbySubsId(user: User, body): Promise<any> {
    // SQL -- SELECT mail FROM users,subscriptions WHERE users.id = subscriptions.iduser AND subscriptions.job_profile=3 AND subscriptions.codml=2;
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport({
          host: 'stradino2.gruppoifi.com', //gmail
          port: 25,
          authMethod: 'NTLM',
          secure: false,
          tls: { rejectUnauthorized: false },
          debug: true,
        });
        body.from = '"Exor" <subscriptions@exor.com>';
        body.to += ', subscriptions@exor.com';
        var mailOptions = body;
        /* transporter.sendMail(mailOptions, function (error, info) {
           if (error) {
             console.log(error);
             resolve({ 'mail_sended': false });
           } else {
             console.log('Email sent: ' + info.response);
             resolve({ 'mail_sended': true });
           }
         });*/
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static sendMail(user: User, body): Promise<any> {
    // SQL -- SELECT mail FROM users,subscriptions WHERE users.id = subscriptions.iduser AND subscriptions.job_profile=3 AND subscriptions.codml=2;
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        let sql;
        console.log('Send mail data: ', body);
        if(!body.job_profile) {
           sql = `SELECT mail FROM users,subscriptions WHERE users.id = subscriptions.iduser AND subscriptions.codml=${body.codml}`;
        } else {
          sql = `SELECT mail FROM users,subscriptions,job_profile WHERE users.id = subscriptions.iduser AND subscriptions.job_profile=${body.job_profile} AND subscriptions.codml=${body.codml} AND job_profile.id =${body.job_profile} AND job_profile.codml = ${body.codml}`;
        }
        console.log('SQL: ', sql);
        console.log('Params: ', body);
        pool.query(sql, [body.job_profile, body.codml, body.job_profile, body.codml], function (err, result, fields) {
          if (err) {
            console.log('query mysql error.');
            reject({ error: err, status: 500 });
          } else {
            if(result.length>0){
              body.to = 'subscriptions@exor.com'; 
              for (let i in result) {
                body.to += ', ' + result[i].mail;
              }
              console.log('Users to send: ', body);
              var nodemailer = require('nodemailer');
              var transporter = nodemailer.createTransport({
                host: 'stradino2.gruppoifi.com', //gmail
                port: 25,
                authMethod: 'NTLM',
                secure: false,
                tls: { rejectUnauthorized: false },
                debug: true,
              });
              /*var transporter = nodemailer.createTransport({ DEV
                service: 'gmail',
                auth: {
                  user: 'togometricsdevs@gmail.com',
                  pass: '!togo2018'
                }
              });*/
              body.from = '"Exor" <subscriptions@exor.com>';
              body.html += `<br><br><span font-style="italic">
              Please note that you can unsubscribe anytime through the following link: <a href="https://wwwtools.exor.com/mail/serveunsubscribe/${body.codml}" target="">‘Unsubscribe’</a>.”
              </span>`
              console.log('TO: ', body.to);
              delete body.job_profile;
              delete body.codml;
              var mailOptions = body;
              transporter.sendMail(mailOptions, function (error, info) {
                 if (error) {
                   console.log('Error mail sent, console log error for debug.');
                   resolve({ 'mail_sended': false });
                 } else {
                   console.log('Email sent: ' + info.response);
                   resolve({ 'mail_sended': true });
                 }
               });
              //resolve(mailOptions);
            } else {
              reject({error: 'no users found', status:406})
            }

          }
        });

      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static addMailingList(user: User, body): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        let mailname = body.name;
        var sql = "INSERT INTO `mailing_list` (`name`) VALUES (?)";
        pool.query(sql, [mailname], function (err, result, fields) {
          if (err) {
            console.log('query mysql error.', err);
            reject({ error: err, status: 500 });
          } else {
            resolve({ 'mailing_list': 'added' });
          } // else query
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static deleteMailingList(user: User, idMailingList: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        var sql = "DELETE FROM `mailing_list` WHERE id = ?";
        pool.query(sql, [idMailingList], function (err, result, fields) {
          if (err) {
            let error = JSON.stringify(err);
            if (error.search(/codml_id/g) !== -1) {
              reject({ error: 'The mailing list you are trying to delete is used by registered users. you can\'t delete it', status: 406 })
            } else {
              console.log('query mysql error.');
              reject({ error: err, status: 400 });
            }

          } else {
            resolve({ 'mailing_list': 'deleted' });
            console.log('mailing_list deleted');
          }//else query
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static unsubscribe(request, user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        var sql = "DELETE FROM subscriptions WHERE subscriptions.user_id= ? && subscriptions.codml= ?";
        console.log('Sql: ', sql, '- params: ', request.params.user_id, ',', request.params.mailing_list);
        pool.query(sql, [request.params.user_id, request.params.mailing_list], function (err, result, fields) {
          if (err) {
            console.log('query mysql error,', err);
            reject({ error: err, status: 500 });
          } else {
            resolve({ 'delete': 'ok' });
          }// else query
        });
      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }

  static generalUnsubscribe(request, user: User): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (user.auth) {
        var sql1 = `SELECT id FROM users WHERE mail = ?`;
        pool.query(sql1, [request.params.mail], function (err, result, fields) {
         if(result.length>0) {
          var sql = "DELETE FROM subscriptions WHERE subscriptions.iduser= ? AND subscriptions.codml = ?";
          console.log(sql);
          pool.query(sql, [result[0].id, request.params.mailing_list], function (err, result, fields) {
            if (err) {
              console.log('query mysql error,', err);
              reject({ error: err, status: 500 });
            } else {
              resolve({ 'delete': 'ok' });
            }// else query
          });
          } else {
            reject({ error: 'no user to unsubscribe', status: 404 });
          }
        });

      } else {
        reject({
          error: 'Unhatorized',
          status: 401
        });
      }
    });
  }
}