
function save() {
    var email_1 = document.getElementById('email_1');
    var email_2 = document.getElementById('email_2');
    if (email_1 === email_2) {
        // CALL REQUEST RESET PASSWORD
        var body = {
            'email': email_1
        };

        $.ajax({
            contentType: 'application/json',
            body: {
                "email": email_1
            },
            dataType: 'json',
            success: function (body) {
                app.log("device control succeeded");
            },
            error: function () {
                app.log("Device control failed");
            },
            processData: false,
            type: 'POST',
            url: 'https://dwh.serfit.eu/user/reset'
        });
    } else {
        alert('Email do not match');
    }
}