import { User } from "../entity/User";

let pool = require("../services/database");

export class statisticsService {

    static copyTest(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getAllViews(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                pool.query("SELECT COUNT(id) AS nviews FROM views", function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.');
                        reject({ error: err, status: 500 });
                    } else {
                        if (Object.keys(result).length > 0) {
                            resolve(result[Object.keys(result)[0]]);
                        } else {
                            resolve({ 'result': 0 });
                        }
                    }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static addView(user: User, body): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var date = Date.now();
                pool.query("INSERT INTO views (id,email,date) VALUES (NULL, ?, ?) ", [body.email, date], function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.'); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        resolve();
                    }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getViewsDate(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                pool.query("SELECT date,COUNT(id) AS nviews FROM views GROUP BY date", function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.'); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        if (Object.keys(result).length > 0) {
                            resolve(result);
                        } else {
                            resolve({ 'result': 0 });
                        }
                    }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getReg(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                pool.query("SELECT COUNT(id) AS nregs FROM users WHERE role=0", function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.');
                        reject({ error: err, status: 500 });
                    } else {
                        if (Object.keys(result).length > 0) {
                            resolve(result[Object.keys(result)[0]]);
                        } else {
                            resolve({ 'result': 0 });
                        }
                    }
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getRegsDate(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                pool.query("SELECT reg_date,COUNT(id) AS nregs FROM users WHERE role=0 GROUP BY reg_date", function (err, result, fields) {
                    if (err){
                        console.log('query mysql error.'); // consol log err for more details
                        reject({ error: err, status: 500 });
                      } else {
                    if (Object.keys(result).length > 0) {
                        resolve(result);
                    } else {
                        resolve({'result': 0 });
                    }
                }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

}