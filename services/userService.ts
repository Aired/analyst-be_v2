import { NextFunction } from "express";
import { User } from "../entity/User";
import { UserController } from "controller/userController";
import bodyParser = require("body-parser");

let pool = require("../services/database");
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'd6F3Efeq';

export class userService {



    static encrypt(text) {
        var cipher = crypto.createCipher(algorithm, password)
        var crypted = cipher.update(text, 'utf8', 'hex')
        crypted += cipher.final('hex');
        return crypted;
    }
    static decrypt(text) {
        var decipher = crypto.createDecipher(algorithm, password)
        var dec = decipher.update(text, 'hex', 'utf8')
        dec += decipher.final('utf8');
        return dec;
    }

    static test(request: Request, response: Response, next: NextFunction, user: User, options: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                pool.query('SELECT * FROM users WHERE id=?', [user.id], (err, result, fields) => {
                    if (err) reject({ error: err, status: 500 })
                    else {
                        console.log('User call test: ', user);
                        delete result[0].token;
                        resolve(result);
                    }
                });
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*
    * NEW CREATE USER
    */
    static createUser(token: String): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            try {
                pool.query('SELECT id FROM attempt_activation WHERE token=?', [token], (err, result, fields) => {
                    if (err) reject({ error: err, status: 500 })
                    else {
                        //console.log('Result activation: ', result);
                        var data = userService.decrypt(token);
                        var user_data = data.split('|');
                        //console.log('Decrypt: ', user_data);
                        let mail = user_data[2];
                        console.log('Mail: ', mail);
                        let company = user_data[3];
                        let mailingList = user_data[5];
                        let job_profile = user_data[4];
                        pool.query('SELECT id FROM users WHERE mail=?', [mail], function (err, result) {
                            if (err) {
                                console.log('err: ', err);
                            }
                            console.log('mail: ', mail);
                            console.log('Wtf: ', result);
                            if (result.length > 0) {
                                // insert subscritption (with job profile)
                                //var sql = "INSERT INTO subscriptions (iduser, codml, job_profile, date) SELECT * FROM (SELECT ? AS iduser1, ? AS idmail, ?, ? ) AS tmp WHERE NOT EXISTS (SELECT iduser FROM subscriptions WHERE iduser = iduser1 AND codml = idmail) LIMIT 1;"
                                //POOL QUERY insert subscription first time reg
                                var sql = `INSERT INTO subscriptions (iduser, codml, job_profile, company) VALUES (?,?,?,?)`;
                                console.log('sql: ', sql);
                                let user_id = result[0].id;
                                pool.query(sql, [user_id, mailingList, job_profile, company], function (err, result, fields) {
                                    if (err) {
                                        reject({ error: err, status: 500 });
                                    } else {
                                        let mailOptions = {
                                            from: '',
                                            to:'',
                                            html: ''
                                        }
                                        var nodemailer = require('nodemailer');
                                        var transporter = nodemailer.createTransport({
                                          service: 'gmail',
                                          auth: {
                                            user: 'togometricsdevs@gmail.com',
                                            pass: '!togo2018'
                                          }
                                        });
                                        //body.from = '"Exor" <subscriptions@exor.com>';
                                        mailOptions.from = '"DEVS" <togometricsdevs@gmail.com>';
                                        mailOptions.to = mail;
                                        // body.to += ', subscriptions@exor.com';
                                        //body.to = 'Destinatari da aggiungere';
                                        mailOptions.html += `<br><br><span font-style="italic">
                                        “Thank you for your interest in EXOR.
                                        We confirm you have been added to EXOR's Newsletter and will now be able to receive all
                                        relevant news and press releases.
                                        Please note that you can unsubscribe anytime through the following link: ” <br>
                                        - <a href="https://wwwtools.exor.com/mail/serveunsubscribe/${mailingList}" target="">Unsubscribe</a>.”
                                        </span>`
                                        console.log('TO: ', mailOptions.to);
                                        transporter.sendMail(mailOptions, function (error, info) {
                                           if (error) {
                                             console.log(error);
                                             resolve({ 'mail_sended': false });
                                           } else {
                                             console.log('Email sent: ' + info.response);
                                             resolve({
                                                'mailing_list': mailingList,
                                                'job_profile': job_profile,
                                                'id_user': user_id,
                                                'company': company
                                            });
                                           }
                                         });
                                        /*resolve({
                                            'mailing_list': mailingList,
                                            'job_profile': job_profile,
                                            'id_user': user_id,
                                            'company': company
                                        });*/
                                    }
                                });
                            } else {
                                var date = new Date();
                                // user _data mi arriva dall'altra chiamata
                                var sql = "INSERT INTO `users` (`name`, `surname`, `mail`, `company`, `role`, `reg_date`) VALUES (?, ?, ?, ?, 0, ?)"; //`password`, 
                                pool.query(sql, [user_data[0], user_data[1], user_data[2], user_data[3], date], function (err, result, fields) { //userService.encrypt(user_data[2]), 
                                    if (err) {
                                        console.log('query mysql error.', err); // consol log err for more details
                                        reject({ error: err, status: 500 });
                                    } else {
                                        //var sql = "INSERT INTO subscriptions (iduser, codml, job_profile, date) SELECT * FROM (SELECT ? AS iduser1, ? AS idmail, ? AS job_profiles, ?) AS tmp WHERE NOT EXISTS (SELECT iduser FROM subscriptions WHERE iduser = iduser1 AND codml = idmail AND job_profile = job_profiles) LIMIT 1"
                                        //POOL QUERY insert subscription first time reg
                                        var sql = `INSERT INTO subscriptions (iduser, codml, job_profile, company) VALUES (?,?,?,?)`;
                                        console.log(sql + ',' + result.insertId + ',' + mailingList + ',' + date)
                                        pool.query(sql, [result.insertId, mailingList, user_data[4], user_data[3]], function (err, result, fields) {
                                            if (err) {
                                                reject({ error: err, status: 500 });
                                            } else {
                                                let mailOptions = {
                                                    from: '',
                                                    to:'',
                                                    html: ''
                                                };
                                                var nodemailer = require('nodemailer');
                                                var transporter = nodemailer.createTransport({
                                                  service: 'gmail',
                                                  auth: {
                                                    user: 'togometricsdevs@gmail.com',
                                                    pass: '!togo2018'
                                                  }
                                                });
                                                //body.from = '"Exor" <subscriptions@exor.com>';
                                                mailOptions.from = '"DEVS" <togometricsdevs@gmail.com>';
                                                mailOptions.to = mail;
                                                // body.to += ', subscriptions@exor.com';
                                                //body.to = 'Destinatari da aggiungere';
                                                mailOptions.html += `<br><br><span font-style="italic">
                                                “Thank you for your interest in EXOR.
                                                We confirm you have been added to EXOR's Newsletter and will now be able to receive all
                                                relevant news and press releases.
                                                Please note that you can unsubscribe anytime through the following link: ” <br>
                                                - <a href="http://wwwtools.exor.com/mail/serveunsubscribe/${mailingList}" target="">Unsubscribe</a>.”
                                                </span>`
                                                console.log('TO: ', mailOptions.to);
                                                transporter.sendMail(mailOptions, function (error, info) {
                                                   if (error) {
                                                     console.log(error);
                                                     resolve({ 'mail_sended': false });
                                                   } else {
                                                     console.log('Email sent: ' + info.response);
                                                     resolve({
                                                        'mailing_list': mailingList,
                                                        'job_profile': job_profile,
                                                        'id_user': result.insertId,
                                                        'company': company
                                                    });
                                                   }
                                                 });
                                                /*resolve({
                                                    'mailing_list': mailingList,
                                                    'job_profile': user_data[4],
                                                    'id_user': result.insertId
                                                });*/
                                            }
                                        });

                                    } // else query
                                });
                            }
                        })
                    }
                });

            } catch (err) {
                console.log(err);
                reject({ error: err, status: 500 });
            }

        });
    }
    /*-----------------------------------*/

    static deleteUser(user: User, id: number): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "DELETE FROM users WHERE users.id= ? && role!=1"; //" + mail + "
                console.log(sql);
                console.log('userid to delete: ', id);
                pool.query(sql, [id], function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error,', err); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        resolve({ 'delete': 'ok' });
                    }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*--------------------------------------*/

    static getUser(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = `
                SELECT users.id,users.name,users.surname,users.mail,users.company,users.role_company,users.role
                FROM users
                WHERE users.id = ?`;
                pool.query(sql, [user.id], function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.'); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        console.log('no err');
                        if (Object.keys(result).length > 0) {
                            resolve(result);
                        } else {
                            reject({ error: 'no data found', status: 404 });
                        }
                    }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*-----------------------------------*/

    static getAllUser(user: User, codml): Promise<any> {
        let subs = [];
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                console.log('getAllUserService');
                var sql0 = `SELECT DISTINCT users.id,users.name,users.surname,users.mail,users.role,s.job_profile,profile2.name AS job_profile_name,s.company
                            FROM users
                            INNER JOIN subscriptions s on users.id = s.iduser
                            INNER JOIN job_profile profile2 on s.job_profile = profile2.id
                            WHERE s.codml = ? AND users.role=0`;
                pool.query(sql0, [codml], (err, result, fields) => {
                    if (err) {
                        console.log('query mysql error.'); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        //console.log(result);
                        /* var sql = "SELECT codml,iduser FROM subscriptions";
                             pool.query(sql, (err, result2, fields) => {
                                 for(let i in result){
                                 }
                             })*/
                        resolve(result);
                        /*Object.keys(result).forEach((key) => {
                            var sql = "SELECT codml FROM subscriptions WHERE `iduser`= ?";
                            pool.query(sql, [result[key].id], (err, result2, fields) => {
                                if (err) {
                                    console.log('query mysql error.'); // consol log err for more details
                                    reject({ error: err, status: 500 });
                                } else {
                                    var sub = [];
                                    result2.map(
                                        (r) => {
                                            sub.push(r.codml);
                                        }
                                    );
                                    var obj = Object.assign({}, result[key]);
                                    obj.subs = sub;
                                    subs.push(obj);
                                    if (subs.length == result.length) {
                                        resolve(subs);
                                    }
                                }
                            });
                        });*/
                    }// else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*---------------------------*/

    static getUserByUsername(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "SELECT id,name,surname,mail,company,job_profile,role,reg_date FROM users WHERE `token` = ?";
                pool.query(sql, ['token - da vedereeeeeeee'], function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.'); // consol log err for more details
                        resolve({ error: err, status: 500 });
                    } else {
                        if (Object.keys(result).length > 0) {
                            resolve(result[Object.keys(result)[0]]);
                        } else {
                            resolve({ error: 'not found', status: 404 });
                        }
                    }//else query error
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*------------------------------------    ADD ENCRYPT MODULE */

    static loginUser(user: User, body: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                try {
                    var sql = "SELECT id,name,surname,mail,company,role FROM users WHERE `mail` = ? AND `password` = ?";
                    pool.query(sql, [body.email, userService.encrypt(body.password)], function (err, result, fields) {
                        if (err) {
                            console.log('query mysql error.'); // consol log err for more details
                            reject({ error: err, status: 500 });
                        } else {
                            if (result[0] !== null && result[0] !== '' && result[0] !== undefined) {
                                console.log(result);
                                var str = result[0].id;
                                str += '|';
                                str += result[0].name;
                                str += '|';
                                str += result[0].surname;
                                str += '|';
                                str += result[0].mail;
                                str += '|';
                                str += result[0].company;
                                str += '|';
                                str += result[0].role;
                                str += '|';
                                let d = new Date();
                                d.setMinutes(d.getMinutes() + 30);
                                str += d;
                                var token = userService.encrypt(str);
                                var view_date = new Date();
                               /* var sql = "UPDATE users SET `token`= ? WHERE `mail`= ? AND `password`= ?";
                                pool.query(sql, [token, body.email, userService.encrypt(body.password)], function (err, result, fields) {
                                });*/
                                //if(body.email === 'antonello.quattrocchi01@gmail.com'){}
                                var sql = `INSERT INTO views (username) VALUES ('${body.email}')`;
                                pool.query(sql, function (err, result, fields) {
                                    if (err){
                                        console.log('Not insert view, ',err);
                                    }
                                });
                            } else { var token = null; }
                            if (Object.keys(result).length > 0) {
                                console.log('Login effettuato correttamente');
                                resolve({ 'token': token });
                            } else {
                                reject({
                                    error: 'No user exist',
                                    status: 404
                                });
                            }
                        }// else query error
                    });
                } catch (e) {
                    reject({
                        error: e,
                        status: 500
                    })
                }
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static copyTest(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static updateUser(user: User, body: any): Promise<any> {
        console.log('update user');
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                let new_data = body['new_data'];
                    var sql = "UPDATE `users` SET `name`= ?, `surname`= ?, `mail`= ?, `company`= ? WHERE `id`= ?";
                    pool.query(sql, [new_data.name, new_data.surname, new_data.mail, new_data.company, new_data.id], function (err, result, fields) {
                        if (err) {
                            console.log('query mysql error.'); // consol log err for more details
                            reject({ error: err, status: 500 });
                        } else {
                            if (Object.keys(result).length > 0) {
                                resolve({ user: 'updated' });
                            } else {
                                resolve({ error: 'user to update not found', status: 404 });
                            }
                        } //else query error
                    });
              
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static updatePassword(user: User, body: any): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "UPDATE `users` SET `password` = ? WHERE `id` = ?";
                console.log('sql: ', sql);
                console.log('new pwd: ', body.password);
                console.log('id: ', user.id);
                pool.query(sql, [userService.encrypt(body.password), user.id], function (err, result, fields) { //[body.password,iduser],
                    if (err) {
                        console.log(err);
                        reject();
                    } else {
                        console.log(result);
                        console.log(fields);
                        resolve();
                        console.log('password updated');
                    }
                });

            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static checkMail(user: User, email: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                try {
                    var sql = "SELECT COUNT(*) AS nreg FROM users WHERE mail = ?";
                    pool.query(sql, [email], function (err, result, fields) {
                        if (err) {
                            console.log('query mysql error.', err); // consol log err for more details
                            reject({ error: err, status: 500 });
                        } else {
                            if (result[0].nreg > 0) {
                                resolve({ 'email': 'already exist' });
                            } else {
                                resolve({ 'email': 'available' });
                            }
                        } // else query
                    });
                } catch (e) {
                    console.log(e);
                    reject({ error: e, status: 500 });
                }

            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static requestResetPassword(user: User, email): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "SELECT id,name,surname FROM users WHERE mail='" + email + "'";
                pool.query(sql, function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error => ', err);
                        reject({ error: err, status: 500 });
                    } else {
                        console.log('Result query:', result);
                        if (result.length > 0) {
                            var timestamp = new Date().getUTCMilliseconds();
                            var drag = result[0].id + "|" + result[0].nome + "|" + result[0].cognome + "|" + timestamp;
                            var otp = userService.encrypt(drag);
                            // SALVO OTP SU TABELLA
                            var sql = "INSERT INTO reset VALUES (NULL,'" + email + "','" + otp + "')";
                            pool.query(sql, function (err, result, fields) {
                                if (err) {
                                    console.log('query mysql error => ', err);
                                    reject({ error: err, status: 500 });
                                } else {
                                    // INSERISCO OTP NEL URL RESET PWD
                                    var nodemailer = require('nodemailer');

                                    var transporter = nodemailer.createTransport({
                                        host: 'stradino2.gruppoifi.com',
                                        port: 25,
                                        authMethod: 'NTLM',
                                        secure: false,
                                        tls: { rejectUnauthorized: false },
                                        debug: true
                                    });
                                    var mail = {
                                        'from': '"Exor Newsletter" <subscriptions@exor.com>',
                                        'to': email,
                                        'subject': 'Request reset password',
                                        'html': ``,
                                    }
                                    //mail.from = 'aired.direction@gmail.com';
                                    //mail.to = email;
                                    var link = 'https://wwwtools.exor.com/files/requestReset/' + otp;
                                    mail.html = `<html>
                                      <head>
                                      <title>ResetPWD</title>
                                      </head>
                                      <body>
                                      <h1>Exor Newsletter reset password</h1>
                                      <span>
                                      <p> To access the password reset tool click the following link: </p><br>
                                      <p> <a href='`+ link + `' >- link reset password </a></p>
                                      </span>
                                      </body>
                                      </html>`
                                    var mailOptions = mail;
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                            resolve({ 'mail_sended': false });
                                        } else {
                                            console.log('Email sent: ' + info.response);
                                            resolve({ 'mail_sended': true });
                                        }
                                    });
                                }
                            });
                            // INVIO MAIL PER RESET
                        } else {
                            resolve('no_result_found');
                        }
                    }
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*------------------- RESET PASSWORD --------------------*/
    static resetPassword(user: User, otp): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "SELECT * FROM reset WHERE otp='" + otp + "'"; // TO MODIFY
                pool.query(sql, function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.', sql); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        if (result.length > 0) {
                            resolve({ 'otp': 1 });
                        } else {
                            reject({ 'otp': 0 });
                        }
                    } //else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static resetPasswordDef(user: User, otp, password): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "SELECT mail FROM reset WHERE otp='" + otp + "'"; // TO MODIFY
                pool.query(sql, function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.', sql); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        if (result.length > 0) {
                            var email = result[0].mail;
                            // var pwd = encrypt(password);
                            var sql = "UPDATE users set password='" + userService.encrypt(password) + "' WHERE mail='" + email + "'"; // TO MODIFY
                            pool.query(sql, function (err, result, fields) {
                                if (err) {
                                    console.log('query mysql error.', sql); // consol log err for more details
                                    reject({ error: err, status: 500 });
                                } else {
                                    resolve({ 'reset': true });
                                }
                            })
                        } else {
                            reject({ 'reset': false, status: 500 });
                        }
                    } //else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getUsersbyWorkProfile(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                // TO DO - probably deprecated
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*
    * TO REMOVE??
    */
    static userActivation(user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                //TO DO - USER ACTIVATION
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static addJobProfile(request: any, user: User): Promise<any> {
        var body = request.body;
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                // TO DO ADD JOB PROFILE
                var sql = "INSERT INTO job_profile (name,codml) VALUES (?,?)";
                pool.query(sql, [body.name, body.idMailingList], function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.');
                        let error = JSON.stringify(err);
                        if(error.search(/job_profile_name_uindex/g)!== -1){
                            reject({ error: 'The profile you are trying to adding already exist. you can\'t add it', status:406});
                        } else {
                            console.log('query mysql error.'); // consol log err for more details
                            reject({ error: err, status: 500 });
                        }
                    } else {
                        let resp = {
                            "insertId": result.insertId,
                            "message": 'Job profile added'
                        }
                        resolve(resp);
                    }
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    /*
    * TO REMOVE??
    */
    static sendVerificationMail(user: User): Promise<any> {
        let email = ''; // to add real MAIL
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "SELECT id,name,surname FROM users WHERE mail='" + email + "'";
                pool.query(sql, function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error => ', err);
                        reject({ 'error': err });
                    } else {
                        console.log('Result query:', result);
                        if (result.length > 0) {
                            var timestamp = new Date().getUTCMilliseconds();
                            var drag = result[0].id + "|" + result[0].nome + "|" + result[0].cognome + "|" + timestamp;
                            var otp = userService.encrypt(drag);
                            // SALVO OTP SU TABELLA
                            var sql = "INSERT INTO reset VALUES (NULL,'" + email + "','" + otp + "')";
                            pool.query(sql, function (err, result, fields) {
                                if (err) {
                                    console.log('query mysql error => ', err);
                                    reject({ 'error': err });
                                } else {
                                    // INSERISCO OTP NEL URL RESET PWD
                                    var nodemailer = require('nodemailer');

                                    var transporter = nodemailer.createTransport({
                                        host: 'stradino2.gruppoifi.com',
                                        port: 25,
                                        authMethod: 'NTLM',
                                        secure: false,
                                        tls: { rejectUnauthorized: false },
                                        debug: true
                                    });
                                    var mail = {
                                        'from': '"Exor Newsletter" <subscriptions@exor.com>',
                                        'to': email,
                                        'subject': 'Request reset password',
                                        'html': ``,
                                    }
                                    var link = 'https://wwwtools.exor.com/user/activation/' + otp;
                                    mail.html = `<html>
                              <head>
                              <title>Account activation </title>
                              </head>
                              <body>
                              <h1>Exor Newsletter subscription</h1>
                              <span>
                              <p> Activation link: </p><br>
                              <p> <a href='`+ link + `' >- Verify </a></p>
                              </span>
                              </body>
                              </html>`
                                    var mailOptions = mail;
                                    transporter.sendMail(mailOptions, function (error, info) {
                                        if (error) {
                                            console.log(error);
                                            resolve({ 'mail_sended': false });
                                        } else {
                                            console.log('Email sent: ' + info.response);
                                            resolve({ 'mail_sended': true });
                                        }
                                    });
                                }
                            });
                            // INVIO MAIL PER RESET
                        } else {
                            resolve('no_result_found');
                        }
                    }
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static regUser(request: any, user: User): Promise<any> {
        const body = request.body;
        return new Promise<any>((resolve, reject) => {
            let sql = `SELECT users.id from users
            INNER JOIN subscriptions s on users.id = s.iduser
            WHERE users.mail=? and s.codml = ?`;
            pool.query(sql, [body.email, body.idMailingList], function (err, result, fields) {
                if(result.length>0){
                    reject({error: 'Already exist', status:400})
                } else {
                    let data = body.name + '|' + body.surname + '|' + body.email + '|' + body.company + '|' + body.job + '|' + body.idMailingList + '|' + new Date();
                    const token = userService.encrypt(data);
                    pool.query("INSERT INTO attempt_activation (token) VALUES (?) ", [token], function (err, result, fields) {
                        if (err) {
                            console.log('query mysql error.'); // consol log err for more details
                            reject({ error: err, status: 500 });
                        } else {
                            var nodemailer = require('nodemailer');
        
                            var transporter = nodemailer.createTransport({
                                /*host: 'stradino2.gruppoifi.com',
                                port: 25,
                                authMethod: 'NTLM',
                                secure: false,
                                tls: { rejectUnauthorized: false },
                                debug: true
                                host: 'smtp.gmail.com',
                                port: 465,
                                secure: true, // use SSL*/
                                service: 'gmail',
                                auth: {
                                    user: 'togometricsdevs@gmail.com',
                                    pass: '!togo2018'
                                }
                            });
                            var mail = {
                                'from': '"Exor Newsletter" <subscriptions@exor.com>',
                                'to': body.email,
                                'subject': 'User Activation',
                                'html': ``,
                            }
                            //mail.from = 'aired.direction@gmail.com';
                            //mail.to = email;
                            var link = 'http://192.168.1.147:3000/user/activation/' + token;
                            mail.html = `<html>
                                              <head>
                                              <title>User activation</title>
                                              </head>
                                              <body>
                                              <h1>Exor Newsletter User Activation</h1>
                                              <span>
                                              <p> To activate your account click the following link: </p><br>
                                              <p> <a href='`+ link + `' >- link activation </a></p>
                                              </span>
                                              </body>
                                              </html>`
                            var mailOptions = mail;
                            transporter.sendMail(mailOptions, function (error, info) {
                                if (error) {
                                    console.log(error);
                                    reject({ error: error, status: 500 });
                                } else {
                                    console.log('Email sent: ' + info.response);
                                    resolve({ 'mail_sended': true });
                                }
                            });
                            // resolve();
                        }// else query
                    });
                }
            })


        });
    }

    static testMail(): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            var nodemailer = require('nodemailer');

            var transporter = nodemailer.createTransport({
                /*host: 'stradino2.gruppoifi.com',
                port: 25,
                authMethod: 'NTLM',
                secure: false,
                tls: { rejectUnauthorized: false },
                debug: true*/
                service: 'gmail',
                //host: 'smtp.gmail.com',
                //port: 465,
                //secure: false, // use SSL
                auth: {
                    user: 'togometricsdevs@gmail.com',
                    pass: '!togo2018'
                }
            });
            var mail = {
                'from': '"Exor Newsletter" <subscriptions@exor.com>',
                'to': 'antonello.quattrocchi01@gmail.com',
                'subject': 'User Activation',
                'html': ``,
            }
            //mail.from = 'aired.direction@gmail.com';
            //mail.to = email;
            var link = 'https://wwwtools.exor.com/user/activation/';
            mail.html = `<html>
                              <head>
                              <title>User activation</title>
                              </head>
                              <body>
                              <h1>Exor Newsletter User Activation</h1>
                              <span>
                              <p> To activate your account click the following link: </p><br>
                              <p> <a href='`+ link + `' >- link activation </a></p>
                              </span>
                              </body>
                              </html>`
            var mailOptions = mail;
            transporter.sendMail(mailOptions, function (error, info) {
                if (error) {
                    console.log(error);
                    reject({ error: error, status: 500 });
                } else {
                    console.log('Email sent: ' + info.response);
                    resolve({ 'mail_sended': true });
                }
            });

        })
    }

    static getJobProfile(request: any, user: User): Promise<any> {
        console.log('Request params: ', request.params);
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                pool.query('SELECT id,name,codml FROM job_profile', (err, result, fields) => {
                    if (err) reject({ error: err, status: 500 })
                    else {
                        resolve(result);
                    }
                });
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getAdminUsers(request: any, user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
           // console.log('What: ',user);
            if (user.auth) {
                pool.query('SELECT id,name,surname,mail,company,reg_date FROM users WHERE role=1 AND mail!=?',[user.email], (err, result, fields) => { // add !=antonello.quattrocchi01@gmail.com
                    if (err) reject({ error: err, status: 500 })
                    else {
                        resolve(result);
                    }
                });
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static createAdminUser(request: any, user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                console.log(request.body);
                let body = request.body;
                let reg_date = new Date();
                var sql = "INSERT INTO `users` (`name`, `surname`,  `password`, `mail`, `company`,`role`, `reg_date`) VALUES (?, ?, ?, ?, ?, 1, ?)"; //`password`, 
                pool.query(sql, [body.name, body.surname, userService.encrypt(body.password),body.mail, body.company, reg_date], function (err, result, fields) { //userService.encrypt(user_data[2]), 
                    if (err) {
                        console.log('query mysql error.', err); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        resolve(result);
                    }
                });
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static updateAdminUser(request: any, user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                console.log(request.body);
                let body = request.body;
                let new_data = body['new_data'];
                let old_data = body['old_data'];
                let sql;
               
                if(new_data.password){
                    console.log(new_data.password);
                    sql = "UPDATE `users` SET `name`= '"+new_data.name+"', `surname`= '"+new_data.surname+"',`password`= '"+userService.encrypt(new_data.password)+"', `mail`= '"+new_data.mail+"', `company`= '"+new_data.company+"' WHERE `mail`= '"+old_data.mail+"' AND `role`= 1";
                } else {
                    sql = "UPDATE `users` SET `name`= '"+new_data.name+"', `surname`= '"+new_data.surname+"', `mail`= '"+new_data.mail+"', `company`= '"+new_data.company+"' WHERE `mail`= '"+old_data.mail+"' AND `role`= 1";
                }
                console.log(sql);
                
                pool.query(sql, function (err, result, fields) { //userService.encrypt(user_data[2]), 
                    if (err) {
                        console.log('query mysql error.', err); // consol log err for more details
                        reject({ error: err, status: 400 });
                    } else {
                        resolve(result);
                    }
                });
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static deleteAdminUser(request: any, user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                console.log(request.headers.id);
                var sql = "DELETE FROM users WHERE users.id=? AND users.role!=0"; //`password`, 
                pool.query(sql, [Number(request.headers.id)], function (err, result, fields) { //userService.encrypt(user_data[2]), 
                    if (err) {
                        console.log('query mysql error.', err); // consol log err for more details
                        reject({ error: err, status: 400 });
                    } else {
                        resolve(result);
                    }
                });
            }
            else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static deleteJobProfile(id_job_profile, user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "DELETE FROM `job_profile` WHERE id = ?";
                console.log('Job profile to delete -> ', id_job_profile);
                console.log(sql);
                pool.query(sql, [id_job_profile], function (err, result, fields) {
                    if (err) {
                        //console.log(err);
                        let error = JSON.stringify(err);
                        if(error.search(/jb_fk/g)!== -1){
                            reject({ error: 'The profile you are trying to delete is used by registered users. you can\'t delete it', status:406})
                        } else {
                            console.log('query mysql error.'); // consol log err for more details
                            reject({ error: err, status: 500 });
                        }
                        
                    } else {
                        resolve({ 'job_profile': 'deleted' });
                        console.log('job_profile deleted');
                    }//else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }

    static getUserProcedure(request: any, user: User): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            if (user.auth) {
                var sql = "CALL listUsers()";
                pool.query(sql, function (err, result, fields) {
                    if (err) {
                        console.log('query mysql error.'); // consol log err for more details
                        reject({ error: err, status: 500 });
                    } else {
                        resolve(result);
                        console.log(result);
                    }//else query
                });
            } else {
                reject({
                    error: 'Unhatorized',
                    status: 401
                });
            }
        });
    }
}
